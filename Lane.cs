﻿using Microsoft.Xna.Framework;

namespace Новая_папка
{
    /// <summary>
    /// Структура хранящая данные для рисования кривой линии.
    /// </summary>
    struct Lane
    {
        /// <summary>
        /// Позиция начальной точки кривой (x,y)
        /// </summary>
        public Vector2 start;
        /// <summary>
        /// Позиция вершины кривой.
        /// </summary>
        public Vector2 end;
        /// <summary>
        /// Позиция конечной вершины кривой.
        /// </summary>
        public Vector2 middl;
        /// <summary>
        /// Индекс первой частицы в массиве частиц с которой начнеться рисование линии.
        /// </summary>
        public int arStart;
        /// <summary>
        /// Индекс последней частицы в массиве мачастиц.
        /// </summary>
        public int arEnd;
    }
}